# A function to find the top n-th most frequent words in a text file
def find_nth_frequent_words(filename, n):
    """
    filename: txt file ('xxxx.txt'), the file name that is used and its direction；
    n: int, the n-th most frequent word you want to find；
    """

    file = open(filename, 'r')

    # Count word frequency
    counts = dict()
    for line in file:
        words = line.split()
        for word in words:
            counts[word] = counts.get(word, 0) + 1

    # Store the word and count into a list of tuples
    lst = list()
    for word, count in counts.items():
        tup = (count, word)
        lst.append(tup)

    # Sort the list according to count
    lst = sorted(lst, reverse=True)

    # Return the nth frequent word and its count
    output = dict()
    for count, word in lst[0:n]:
        output[word] = count
    print('Find the top nth most frequent words finished!')
    return output

# A function to fill missing values in a panel data set
def panel_data_fill(filename,time,entity,values):
    """
    filename: the file that is used to fill；
    time: str, time series index column；
    entity: str, entity index column；
    values: list of str, the list of columns that may have missing values；
    Rules of filling： If there are missing values in a group, fill with linear interpolation;
                    if the missing value is the first or last value in a group, fill with the previous/latter value;
                    if the whole group is missing, fill with the average of other entity values;
    """
    import pandas as pd
    data = pd.read_csv(filename)
    data = data.sort_values(by=[time,entity]).reset_index(drop=True)
    for value in values:
        if data[value].isna().sum()==0:
            print("There is no missing value in the original data.")
        else:
            missing_sum=data[value].isna().sum()
            data_group=data.groupby(entity)
            # If there are missing values in a group, fill with linear interpolation
            # If the missing value is the first or last value in a group, fill with the previous/latter value
            data=data_group.transform(lambda x: x.interpolate(method='linear', limit_direction='both')).join(data.reset_index()[entity])
            if data[value].isna().sum()==0:
                print("The number of missing value in the original data is:",missing_sum,"，filled.")
            else:
                # If the whole group is missing, fill with the average of other entity values
                data=data[[time,entity]].join(data.groupby(time)[value].transform(lambda x: x.fillna(x.mean())).reset_index(drop=True))
                if data[value].isna().sum()==0:
                    print("There is a group where all values are missing. Filled.")
                else:
                    print("There are still missing values after filling.")
    print('Panel data filling finished!')
    return data


# A function to calculate VWAP of stocks
def calculate_VWAP(filename, sec_column, price_column, vol_column):
    """
    filename: csv file ('xxxx.csv'), the file name that is used to calculate VWAP and its direction；
    sec_col: str, the column indicating the security code；
    price_col: str, the column indicating the stock closing price；
    vol_col: str, the column indicating the trading volume；
    Calculation： VWAP = sum(closingPrice*tradeVolume)/sum(tradeVolume)；
    The results are written in a csv 'filename_VWAP.csv'；
    """

    import csv

    # Store the security code and total volume in a dictionary; key = security code, value = total volume
    # Store the security code and total dollar value in a dictionary; key = security code, value = total dollar value
    total_volume = dict()
    total_dollar_value = dict()

    file = open(filename, 'r')
    reader = csv.DictReader(file)

    for row in reader:
        sec = row[sec_column]
        price = row[price_column]
        vol = row[vol_column]

        if (vol != 0 and vol != '') and (price != 0 and price != ''):
            total_dollar_value[sec] = total_dollar_value.get(sec, 0) + float(price) * float(vol)
            total_volume[sec] = total_volume.get(sec, 0) + float(vol)

    # Write retulsts to csv
    with open('VWAP.csv', mode="w") as file:
        vwap_file = csv.writer(file)
        header = ['security', 'total_dollar_value', 'total_volume', 'VWAP']
        vwap_file.writerow(header)
        for sec in total_volume:
            if total_volume[sec] and total_dollar_value[sec]:
                dollar_value = total_dollar_value[sec]
                volume = total_volume[sec]
                vwap = dollar_value / volume # Calculate VWAP = sum(price*volume)/sum(volume)
                vwap_file.writerow([sec, dollar_value, volume, vwap])
            else:  # It is possble that there is a security has no valid price
                vwap_file.writerow([sec, None, None, None])

    print('Calculating VWAP finished!')
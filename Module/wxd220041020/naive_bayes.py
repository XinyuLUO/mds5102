#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# author: Wang,Xudong 220041020 SDS time:2020/12/14

# NaiveBayes Likelihood estimation
import pandas as pd
import numpy as np

class NaiveBayes(object):
    def getTrainSet(self):

        dataSet = pd.read_csv('./naivebayes_data.csv')
        dataSetNP = np.array(dataSet)
        trainData = dataSetNP[:,0:dataSetNP.shape[1]-1]
        labels = dataSetNP[:,dataSetNP.shape[1]-1]
        return trainData, labels

    def classify(self, trainData, labels, features):

        labels = list(labels)
        P_y = {}
        for label in labels:
            P_y[label] = labels.count(label)/float(len(labels))
        P_xy = {}
        for y in P_y.keys():
            y_index = [i for i, label in enumerate(labels) if label == y]
            for j in range(len(features)):
                x_index = [i for i, feature in enumerate(trainData[:,j]) if feature == features[j]]
                xy_count = len(set(x_index) & set(y_index))
                pkey = str(features[j]) + '*' + str(y)
                P_xy[pkey] = xy_count / float(len(labels))
        P = {}
        for y in P_y.keys():
            for x in features:
                pkey = str(x) + '|' + str(y)
                P[pkey] = P_xy[str(x)+'*'+str(y)] / float(P_y[y])
        F = {}
        for y in P_y:
            F[y] = P_y[y]
            for x in features:
                F[y] = F[y]*P[str(x)+'|'+str(y)]
        features_label = max(F, key=F.get)
        return features_label


if __name__ == '__main__':
    nb = NaiveBayes()
    # traing dataset
    trainData, labels = nb.getTrainSet()
    # x1,x2
    features = [2, 'S']
    # output
    result = nb.classify(trainData, labels, features)
    print(features, 'belong to', result)
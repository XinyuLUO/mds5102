# Fibonacci numbers module

def fib(n):    # write Fibonacci series up to n
    a, b = 0, 1
    while a < n:
        print(a, end=' ')
        a, b = b, a+b
    print()

def fib2(n):   # return Fibonacci series up to n
    result = []
    a, b = 0, 1
    while a < n:
        result.append(a)
        a, b = b, a+b
    return result


def openFileAndCount(path):

    file = open(path,'r')

    # Count word frequency
    # from collections import defaultdict
    # counts = defaultdict(str)
    counts = dict()
    for line in file:
        words = line.split()
        for word in words:
            counts[word] = counts.get(word,0) + 1
    #         counts[word] += 1
    #         print(type(word))
    #         print(type(words))

    # Find the most frequent word
    print(counts)
    maxcount = None
    maxword = None
    for word,count in counts.items():
        if maxcount is None or count > maxcount:
            maxword = word
            maxcount = count

    # Conclusion
    print("The most frequent word is: ", maxword, " and the number of times it appears is: ", maxcount)
